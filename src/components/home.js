import React from 'react';

export default class Home extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         data: {
    //             name: "veera",
    //         }
    //     }
    // }

    // constructor(props) {
    //     super(props);
    //     console.log(this.props);
    //   }\

    // constructor(){
    //     console.log(this) //Error: 'this' is not allowed before super()

    // }
    constructor(props) {
        super(props);
        console.log(this.props); // prints out whatever is inside props
        this.state = {
            data: [{ name: "veera" }, { name: "raj" }]
        }
    }
    // componentWillMount() {
    //     console.log("componentWillMount")
    // }
    // componentDidMount() {
    //     console.log("componentDidMount")
    // }

    // componentWillReceiveProps() {
    //     console.log("componentWillReceiveProps")
    // }

    // componentWillUpdate() {
    //     console.log("componentWillUpdate")

    // }
    // componentDidUpdate() {
    //     console.log("componentDidUpdate")

    // }

    // componentWillUnmount() {
    //     console.log("componentWillUnmount")

    // }
    render() {
        return (
            <div>
                <ul>
                    {this.state.data.map((item, index) => (
                        <li key={index} >{item.name}</li>
                    ))}
                </ul>
                <Banner homeProb={"test"}></Banner>
            </div>
        );
    }
}


class Banner extends React.Component{
    constructor(props) {
        super(props);
        console.log(this.props);
    }

    componentWillReceiveProps() {
        console.log("componentWillReceiveProps")
    }
    render() { 
        return (
            <div>
                
            </div>
        )
    }
}